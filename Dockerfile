FROM alpine

ARG plugins='dyndns,tls.dns.cloudflare'

RUN apk add bash curl

RUN curl https://getcaddy.com | bash -s personal dyndns,tls.dns.cloudflare


ENTRYPOINT ["/usr/local/bin/caddy"]
CMD ["-conf", "/etc/Caddyfile", "-log", "stdout", "-agree=true"]